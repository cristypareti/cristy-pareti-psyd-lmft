I am licensed as a Marriage and Family Therapist in the State of California (LMFT #47538). I have both a Masters and a Doctorate in Clinical Psychology from The American School of Professional Psychology, an APA accredited program. I also completed a specialty track in Child and Adolescent Psychology.

Address: 1500 Rosecrans Avenue, Suite 500, Manhattan Beach, CA 90266, USA

Phone: 310-571-5936

Website: http://www.cristypareti.com
